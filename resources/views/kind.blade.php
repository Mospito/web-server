<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">


<div class="text-center">
        <h1> This is Kind Page!! </h1>
</div>

<table class="table table-striped table-hover">
<tr>
<th>kind</th>
</tr>
@forelse ($kind as $item)
<tr>
    <td><a href=" {{ route('list', ['kind' => $item]) }} ">{{ $item }}</a></td>
</tr>
@empty
<tr><td> No items </td></tr>
@endforelse
</table>

<br/>
<div  class="text-center">
	<a href=" {{ route('index') }} "><button type="button" class="btn btn-outline-primary"> index page</button> </a>
</div>
