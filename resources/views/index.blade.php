<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">


<div style="color:red;">
	<h1 class="text-center">Show All Data!! </h3>
	<h2 class="text-center"> The number of All Data is: {{ $items->count() }} </h2>
</div>
<div style="text-align:center;">

	<table class="table table-striped">
	<thead>
	<tr>
	<th scope="col">Timestamp</th>
	<th scope="col">ID</th>
	<th scope="col">Found</th>
	</tr>
	</thead>
	<tbody>
	@forelse ($items as $item)
	<tr>
	    <td>{{ $item->created_at }}</td>
	    <td>{{ $item->id }}</td>
	    <td>{{ $item->found }}</td>
	</tr>


	@empty
	<tr><td> No items </td></tr>
	@endforelse
	</tbody>
	</table>

	<div style="text-align:center;"> 


		<a href=" {{ route('kind') }} "> <button type="button" class="btn btn-outline-primary">kind page</button></a>
		<a href=" {{ route('lemon')}} "><button type="button" class="btn btn-outline-danger">Find Lemons!!</button></a>
		<a href=" {{ route('notlemon')}} "><button type="button" class="btn btn-outline-warning"> Find With out Lemon!!</button></a>
		 <a href=" {{ route('chart')}} "><button type="button" class="btn btn-outline-success"> Chart!!</button></a>
		<a href=" {{ route('users')}} "><button type="button" class="btn btn-outline-info"> Users</button></a>

	</div>

</div>
