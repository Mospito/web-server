<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">



<table class="table table-striped table-hover">
<tr>
<th>timestamp</th>
<th>found</th>
<th>qty</th>
</tr>
@forelse ($items as $item)
<tr>
    <td>{{ $item->created_at }}</td>
    <td>{{ $item->found }}</td>
    <td>{{ $item->qty }}</td>
</tr>
@empty
<tr><td> No items </td></tr>
@endforelse
</table>


<br/>
<a href=" {{ route('index') }} "><button type="button" class="btn btn-outline-primary">index page </button></a>
<a href=" {{ route('kind') }} "><button type="button" class="btn btn-outline-secondary">kind page </button></a>

