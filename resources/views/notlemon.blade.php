<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">

<div class="text-center">
	<h1> This is Not Lemon!! </h1>
	<h2> The number of  Not Lemon is: {{ $items->count() }} </h2>
</div>

<table class="table table-striped table-hover">
<tr>
<th>timestamp</th>
<th>found</th>
</tr>
@forelse ($items as $item)
<tr>
    <td>{{ $item->created_at }}</td>
    <td>{{ $item->found }}</td>
</tr>
@empty
<tr><td> No items </td></tr>
@endforelse
</table>


<div class="text-center">
	<a href=" {{ route('index') }} "><button type="button" class="btn btn-outline-primary">index page</button></a>
	<a href=" {{ route('lemon') }} "><button type="button" class="btn btn-outline-danger">Find Lemons!!</button></a>
</div>
