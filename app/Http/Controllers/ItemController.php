<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Items;
use Illuminate\Support\Arr;

class ItemController extends Controller
{

   public function index()
   {
  	/* $item = Items::All(); */
	$items = Items::orderby('created_at', 'desc')->get();
	return view('index', ["items" => $items]);
       // return response()->json($item);

   }



    public function kind()
    {
    /*	$item = Items::All(); */
	$item = Items::groupBy('found')->get(['found']);
	return view('kind',["kind" => Arr::pluck($item, 'found')]);
	//return response()->json(Arr::pluck($item, 'found'));
    }
   
    public function list(Request $request, $kind)
    {
	$item = Items::where('found', $kind)->orderby('created_at','desc')->get();
	//return response()->json($item);
	return view('data', ["items" => $item]);
    }

    public function findLemon ()
    {
	$lemon = Items::where('found', 'lemon')->orderby('created_at','desc')->get();
	return view('lemon', ["lemons" => $lemon]);
    }
    
      public function notLemon ()
    {
        $item = Items::where('found','!=', 'lemon')->orderby('created_at','desc')->get();
        return view('notlemon', ["items" => $item]);
    }


    public function push(Request $request)
    {
	$input = $request->all();
	$item = Items::create($input);
    }
}
