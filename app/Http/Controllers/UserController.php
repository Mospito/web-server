<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;


class UserController extends Controller
{

	public function findUsers()
	{
		$users = User::orderby('created_at','desc')->get();
		return view('users',["users" => $users]);

	}
	

	public function deleteUser(Request $request,$id)
	{
		   $post= User::find($id);
   		   $post->delete();
 		   return redirect('/users/data')->with('success', 'User Removed');
	
	}

}
