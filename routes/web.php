<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TestController;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\ChartController;
use App\Http\Controllers\UserController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});
*/
Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/index', [ItemController::class, 'index'])->name('index');
Route::get('/test', [TestController::class, 'test']);
Route::get('/kind', [ItemController::class, 'kind'])->name('kind');
Route::get('/list/{kind}', [ItemController::class, 'list'])->name('list');
Route::get('/lemon', [ItemController::class, 'findLemon'])->name('lemon');
Route::get('/notlemon', [ItemController::class, 'notLemon'])->name('notlemon');


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/chart', [ChartController::class, 'index'])->name('chart');
Route::get('/users', [UserController::class, 'findUsers'])->name('users');
Route::get('/users/delete', [UserController::class, 'deleteUsers'])->name('deleteUsers');

