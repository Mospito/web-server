<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\User;
use Illuminate\Database\Query\Builder;




class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Jantapa',
            'email' => Str::random(10).'@gmail.com',
            'password' => Hash::make('password'),
	    'fullname' => 'Binheem',
        ]);


	 DB::table('users')->insert([
            'name' => 'Ketsitee',
            'email' => Str::random(10).'@gmail.com',
            'password' => Hash::make('password'),
            'fullname' => 'Tantiwee',
        ]);


	DB::table('users')->insert([
            'name' => 'Arim',
            'email' => Str::random(10).'@gmail.com',
            'password' => Hash::make('password'),
            'fullname' => 'Cheberahim',
        ]);
	 DB::table('users')->insert([
            'name' => 'Jaturon',
            'email' => Str::random(10).'@gmail.com',
            'password' => Hash::make('password'),
            'fullname' => 'Moonjan',
        ]);
	 DB::table('users')->insert([
            'name' => 'Kittipot',
            'email' => Str::random(10).'@gmail.com',
            'password' => Hash::make('password'),
            'fullname' => 'Noothong',
        ]);
    }
}
