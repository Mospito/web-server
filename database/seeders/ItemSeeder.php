<?php

namespace Database\Seeders;
use Illuminate\Support\Facades\DB;



use Illuminate\Database\Seeder;

class ItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('items')->insert([
            'found' => 'Apple',
            'qty' => 1,
           
        ]);

	DB::table('items')->insert([
            'found' => 'Papaya',
            'qty' => 2,

        ]);
	
	 DB::table('items')->insert([
            'found' => 'Banana',
            'qty' => 4,

        ]);



	
    }
}
