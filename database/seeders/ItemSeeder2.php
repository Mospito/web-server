<?php

namespace Database\Seeders;
use App\Models\Items;



use Illuminate\Database\Seeder;

class ItemSeeder2 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	$item = new Items();
	$item->found = "lime";
	$item->qty = 99;
	$item->save();
    }
}
